Interactive GCC
===============

Interactive GCC (igcc) is a read-eval-print loop (REPL) for C/C++ programmers.

It can be used like this:

```bash
$ ./igcc
g++> int a = 5;
g++> a += 2;
g++> cout << a << endl;
7
g++> --a;
g++> cout << a << endl;
6
g++>
```

It is possible to include header files you need like this:

```bash
$ ./igcc
g++> #include <vector>
g++> vector<int> myvec;
g++> myvec.push_back( 17 );
g++> printf( "%d\n", myvec.size() );
1
g++> myvec.push_back( 21 );
g++> printf( "%d\n", myvec.size() );
2
g++>
```

It is possible to include your own functions using '.f':

```bash
g++> .f
Functions paste mode is ON: Enter ".f" again to return to return to normal editing.

g++> int lastIndexOf(char *str, char c) {
g++>   int ind = -1;
g++>  for (int i=strlen(b)-1;i>=0;i--) {
g++>   if (b[i] == c) {
g++>     ind = i;
g++>     break;
g++>   }
g++>  }
g++>  return ind;
g++> }
g++> .f
Functions paste mode is OFF
```

You can use '.p' to enter "Paste" mode to enter multi line snippets :

```bash
g++> .p
Paste mode is ON: Enter ".p" again to return to return to normal editing.

g++> char c = '_';
g++> char *a = (char *)malloc(80);
g++> char b[] = "a_another_word_there_last";
g++> strcpy(a, b[lastIndexOf('_'));
g++> puts(a);
g++> .p
Paste mode is OFF

[Compile error - type .e to see it.]
```


Compile errors can be tolerated until the code works:

```bash
$ ./igcc
g++> #include <map>
g++> map<string,int> hits;
g++> hits["foo"] = 12;
g++> hits["bar"] = 15;
g++> for( map<string,int>::iterator it = hits.begin(); it != hits.end(); ++it )
[Compile error - type .e to see it.]
g++> {
[Compile error - type .e to see it.]
g++> 	cout << it->first << " " << it->second << endl;
[Compile error - type .e to see it.]
g++> }
bar 15
foo 12
g++>
```

Extra include directories can be supplied:

```bash
$ ./igcc -Itest/cpp -Itest/cpp2
g++> #include "hello.h"
g++> hello();
Hello,
g++> #include "world.h"
g++> world();
world!
g++>
```

Libs can be linked:

```bash
$ ./igcc -lm
g++> #include "math.h"
g++> cout << pow( 3, 3 ) << endl; // Actually a bad example since libm.a is already linked in C++
27
g++>
```

Your own libs can be linked too:

```
$ ./igcc -Itest/cpp -Ltest/cpp -lmylib
g++> #include "mylib.h"
g++> defined_in_cpp();
defined_in_cpp saying hello.
g++>
```

The cstdio, iostream and string headers are automatically included, and the std namespace is automatically in scope.

Copyright
---------

Copyright (C) 2009 Andy Balaam
Copyright (C) 2019 Zaoqi

IGCC is Free Software released under the terms of the GNU General Public License version 2 or later.

IGCC comes with NO WARRANTY.

See the file LICENSE for more information.

